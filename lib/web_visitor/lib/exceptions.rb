
module WebVisitor

  class NoPageObjectFound < NoMethodError; end
  class NoElementInActualPageObject < NoMethodError; end

end