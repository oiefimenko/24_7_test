
class MyPage < WebVisitor::Page

  page_url BASE_URL + '/my/page'
  uri_matcher /\/my\/page/

  link :my_issue, xpath: '//*[@class="list issues"]/.//a[1]'

  # Header
  link :home, xpath: '//*[@class="home"]'
  link :project, xpath: '//*[@class="projects"]'
  link :sign_out, xpath: '//*[@class="logout"]'
  div :logged_as, xpath: '//*[@id="loggedas"]'

end