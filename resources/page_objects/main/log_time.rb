
class LogTime < WebVisitor::Page

  uri_matcher /\/issues\/\w+\/time_entries\/new/

  text_field :hours, xpath: '//*[@id="time_entry_hours"]'
  select_list :activity, xpath: '//*[@id="time_entry_activity_id"]'
  button :proceed, xpath: '//*[@type="submit"]'

end