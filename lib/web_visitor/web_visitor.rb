
module WebVisitor

  require_relative 'lib/page'
  require_relative 'lib/config'
  require_relative 'lib/crawler'
  require_relative 'lib/exceptions'

  class << self

    def create
      Crawler.new
    end

  end

end