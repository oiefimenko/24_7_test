
class CreateIssue < WebVisitor::Page

  uri_matcher /\/projects\/\w+\/issues\/new/

  select_list :issue_type, xpath: '//*[@id="issue_tracker_id"]'
  text_field :subject, xpath: '//*[@id="issue_subject"]'
  text_field :description, xpath: '//*[@id="issue_description"]'
  select_list :assign_to, xpath: '//*[@id="issue_assigned_to_id"]'
  button :complete_issue, xpath: '//*[@type="submit"]'

  # Header
  link :home, xpath: '//*[@class="home"]'
  link :project, xpath: '//*[@class="projects"]'
  link :sign_out, xpath: '//*[@class="logout"]'
  div :logged_as, xpath: '//*[@id="loggedas"]'

  def create_random_issue
    self.subject_element.when_present
    self.subject = Faker::Hipster.sentence
    self.description = Faker::Hacker.say_something_smart
    self.complete_issue
    self.complete_issue_element.when_not_present
  end

end