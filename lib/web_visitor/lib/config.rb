
module WebVisitor

  class Config

    class DefaultConfig

      attr_accessor :browser
      attr_accessor :page_change_wait_time
      attr_accessor :page_object_set

      def initialize
        @browser                = :chrome
        @page_change_wait_time  = 15
        @page_object_set        = ''
      end

    end

    class << self

      def method_missing(name, *params)
        @config ||= DefaultConfig.new
        name = name.to_sym
        params.count > 0 ? @config.send(name, params.first) : @config.send(name)
      end

    end

  end

end
