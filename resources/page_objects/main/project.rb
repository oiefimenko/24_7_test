
class Project < WebVisitor::Page

  uri_matcher /\/projects\/(?!new)\w+$/

  div :message, xpath: '//*[@id="flash_notice"]'
  p :warning, xpath: '//*[@class="warning"]'
  link :create_new_issue, xpath: '//*[@class="new-issue"]'
  link :settings, xpath: '//*[@class="settings"]'
  link :close_project, xpath: '//*[@class="icon icon-lock"]'

  # Header
  link :home, xpath: '//*[@class="home"]'
  link :project, xpath: '//*[@class="projects"]'
  link :sign_out, xpath: '//*[@class="logout"]'
  div :logged_as, xpath: '//*[@id="loggedas"]'

end