
module WebVisitor

  class Crawler

    attr_reader :driver

    def initialize
      register_page_objects
    end

    def open(page)
      new_page = @page_objects.values.find { |page_object| valid_name(page_object) == valid_name(page) }
      raise WebVisitor::NoPageObjectFound 'Page Object with such name does not exist!' if new_page.nil?
      @driver ||= Watir::Browser.new(:chrome)
      at_exit { @driver.close }
      @driver.window.maximize
      @current_page = new_page.new(@driver, true)
    end

    def wait_for_page(page)
      wait_for { valid_name(@current_page.class.to_s) == valid_name(page) }
    end

    def wait_for_element(elem)
      wait_for { @current_page.respond_to?(valid_name(elem)) }
    end

    def displayed_page?(page)
      wait_for_page(valid_name(page))
      new_page = @page_objects.values.find { |page_object| valid_name(page_object) == valid_name(page) }
      new_page.new(@driver, false).displayed?
    end

    def method_missing(element, *arg)
      wait_for_element(element)
      unless @current_page.respond_to?(element)
        raise WebVisitor::NoElementInActualPageObject, "#{element} is missing for #{@current_page} page"
      end
      @current_page.send(element, *arg)
    end

    private
    def register_page_objects
      Dir["#{Config.page_object_set}/**/*.rb"].each { |file| require file }
      page_object_classes = ObjectSpace.each_object(Class).select { |page_class| page_class < Page }
      page_object_classes.map! { |page_object| [page_object.uri_matcher, page_object] }
      @page_objects = Hash[page_object_classes]
    end

    def valid_name(element_name)
      element_name.to_s.gsub(' ', '_').downcase.to_sym
    end

    def actualize_page_object
      unless @current_page.displayed?
        current_page = @page_objects.find { |url_regex, value| url_regex.match(@current_page.current_url) }
        @current_page = current_page[1].new(@driver, false)
      end
    end

    def wait_for(&block)
      (Config.page_change_wait_time * 10).times do
        break if yield
        sleep 0.1
        actualize_page_object
      end
    end

  end

end