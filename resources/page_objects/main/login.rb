
class Login < WebVisitor::Page

  page_url BASE_URL + '/login'
  uri_matcher /\/login/

  text_field :login_field, xpath: '//*[@id="username"]'
  text_field :password_field, xpath: '//*[@id="password"]'
  button :enter, xpath: '//*[@type="submit"]'

  def login_with(username:, password:, **keyargs)
    self.login_field_element.when_present
    self.login_field = username
    self.password_field = password
    self.enter
  end

end