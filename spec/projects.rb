describe 'Project:' do

  let!(:user) { @browser.open(Registration); c = @browser.register_random_user; @browser.sign_out; c }

  context 'User' do

    it 'could create a project' do
      @browser.open(Login)
      id = 'p' + Faker::Company.swedish_organisation_number
      @browser.login_with(user)
      @browser.project
      @browser.create_project
      @browser.project_name = Faker::Company.name
      @browser.description = Faker::Company.bs
      @browser.identifier = id
      @browser.save_new_project
      expect(@browser.displayed_page?(ProjectSettings)).to eq(true)
      expect(@browser.current_url).to include(id)
      expect(@browser.message).to include('Successful creation')
    end

    it 'could create an issue under project' do
      @browser.open(Login)
      @browser.login_with(user)
      @browser.project
      @browser.create_project
      @browser.create_random_project
      @browser.create_new_issue
      @browser.subject = Faker::Hipster.sentence
      @browser.description = Faker::Hacker.say_something_smart
      @browser.complete_issue
      expect(@browser.displayed_page?(Issue)).to eq(true)
      expect(@browser.message).to include("Issue ##{@browser.current_url.split('/').last} created")
    end

    it 'could close project' do
      @browser.open(Login)
      @browser.login_with(user)
      @browser.project
      @browser.create_project
      @browser.create_random_project
      @browser.overview
      @browser.close_project
      @browser.driver.alert.ok
      expect(@browser.warning_element.when_present.text).to include('This project is closed and read-only.')
    end

  end

  context 'Management:' do

    let!(:user_2) { @browser.open(Registration); c = @browser.register_random_user; @browser.sign_out; c }

    it 'user can be assigned to project' do
      @browser.open(Login)
      @browser.login_with(user)
      @browser.project
      @browser.create_project
      @browser.create_random_project
      @browser.add_user_to_project(user_2[:username])
      expect(@browser.members_list_elements.length).to eq(2)
    end

    it 'issue can be assigned to user' do
      @browser.open(Login)
      @browser.login_with(user)
      @browser.project
      @browser.create_project
      @browser.create_random_project
      @browser.add_user_to_project(user_2[:username])
      @browser.create_new_issue
      @browser.subject = Faker::Hipster.sentence
      @browser.description = Faker::Hacker.say_something_smart
      @browser.assign_to = "#{user_2[:first_name]} #{user_2[:last_name]}"
      @browser.complete_issue
      expect(@browser.assigned_to_element.when_present.text).to include("#{user_2[:first_name]} #{user_2[:last_name]}")
    end

    it 'user can log time to issue' do
      @browser.open(Login)
      @browser.login_with(user)
      @browser.project
      @browser.create_project
      @browser.create_random_project
      @browser.add_user_to_project(user_2[:username])
      @browser.create_new_issue
      @browser.subject = Faker::Hipster.sentence
      @browser.description = Faker::Hacker.say_something_smart
      @browser.assign_to = "#{user_2[:first_name]} #{user_2[:last_name]}"
      @browser.complete_issue
      @browser.sign_out
      @browser.open(Login)
      @browser.login_with(user_2)
      @browser.my_issue
      @browser.log_time
      @browser.hours = 2
      @browser.activity = 'Design'
      @browser.proceed
      expect(@browser.spent_time_element.when_present.text).to include('2')
    end

    it 'user can close issue' do
      @browser.open(Login)
      @browser.login_with(user)
      @browser.project
      @browser.create_project
      @browser.create_random_project
      @browser.add_user_to_project(user_2[:username])
      @browser.create_new_issue
      @browser.subject = Faker::Hipster.sentence
      @browser.description = Faker::Hacker.say_something_smart
      @browser.assign_to = "#{user_2[:first_name]} #{user_2[:last_name]}"
      @browser.complete_issue
      @browser.sign_out
      @browser.open(Login)
      @browser.login_with(user_2)
      @browser.my_issue
      @browser.edit
      @browser.select_status = 'Closed'
      @browser.save_change
      expect(@browser.status_element.when_present.text).to include('Closed')
    end

  end

end
