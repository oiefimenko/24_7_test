
module WebVisitor

  class Page

    include PageObject

    class << self

      def uri_matcher(*value)
        value.length == 0 ? @matcher : @matcher = value.first
      end

    end

    def displayed?
      current_url.match(self.class.uri_matcher) != nil
    end

  end

end


