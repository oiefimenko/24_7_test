describe 'User:' do

  context 'Registration' do

    it 'could be done by user' do
      @browser.open(Landing)
      @browser.register
      @browser.user_login = Faker::Name.first_name
      @browser.user_password = 'Password'
      @browser.user_password_confirmation = 'Password'
      @browser.user_firstname = Faker::Superhero.name
      @browser.user_lastname = Faker::Hipster.word.capitalize
      @browser.user_mail = Faker::Internet.email
      @browser.user_language = 'English'
      @browser.send_registration_data
      expect(@browser.displayed_page?(MyAccount)).to eq(true)
      expect(@browser.message).to eq('Your account has been activated. You can now log in.')
    end

  end

  context 'Password' do

    it 'could be changed by user' do
      @browser.open(Landing)
      @browser.register
      credentials = @browser.register_random_user
      @browser.change_password
      @browser.old_password = credentials[:password]
      @browser.new_password = credentials[:password] + '1'
      @browser.new_password_confirmation = credentials[:password] + '1'
      @browser.apply_password_change
      @browser.sign_out
      @browser.login
      @browser.login_field = credentials[:username]
      @browser.password_field = credentials[:password] + '1'
      @browser.enter
      expect(@browser.displayed_page?(Landing)).to eq(true)
      expect(@browser.logged_as).to eq("Logged in as #{credentials[:username]}")
    end

  end

end