
class Issue < WebVisitor::Page

  uri_matcher /#{BASE_URL}issues\/\w+$/

  div :message, xpath: '//*[@id="flash_notice"]'
  link :edit, xpath: '//*[@class="icon icon-edit"]'
  link :log_time, xpath: '//*[@class="icon icon-time-add"]'
  link :delete, xpath: '//*[@class="icon icon-del"]'
  select_list :assignee, xpath: '//*[@id="issue_assigned_to_id"]'
  button :save_change, xpath: '//*[@value="Submit"]'
  link :assigned_to, xpath: '//td[@class="assigned-to"]/a'
  link :spent_time, xpath: '//td[@class="spent-time"]/a'
  select_list :select_status, xpath: '//*[@id="issue_status_id"]'
  td :status, xpath: '//td[@class="status"]'

  # Header
  link :home, xpath: '//*[@class="home"]'
  link :project, xpath: '//*[@class="projects"]'
  link :sign_out, xpath: '//*[@class="logout"]'
  div :logged_as, xpath: '//*[@id="loggedas"]'

end