
class CreateProject < WebVisitor::Page

  page_url BASE_URL + 'projects/new'
  uri_matcher /\/projects\/new/

  text_field :project_name, xpath: '//*[@id="project_name"]'
  text_field :description, xpath: '//*[@id="project_description"]'
  text_field :identifier, xpath: '//*[@id="project_identifier"]'
  button :save_new_project, xpath: '//*[@type="submit"]'

  # Header
  link :home, xpath: '//*[@class="home"]'
  link :project, xpath: '//*[@class="projects"]'
  link :sign_out, xpath: '//*[@class="logout"]'
  div :logged_as, xpath: '//*[@id="loggedas"]'

  def create_random_project
    self.project_name_element.when_present
    self.project_name = Faker::Company.name
    self.description = Faker::Company.bs
    self.identifier = 'a' + Faker::Company.swedish_organisation_number
    self.save_new_project
  end

end