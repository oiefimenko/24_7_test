
class Registration < WebVisitor::Page

  page_url BASE_URL + '/account/register'
  uri_matcher /\/account\/register/

  text_field :user_login, xpath: '//*[@id="user_login"]'
  text_field :user_password, xpath: '//*[@id="user_password"]'
  text_field :user_password_confirmation, xpath: '//*[@id="user_password_confirmation"]'
  text_field :user_firstname, xpath: '//*[@id="user_firstname"]'
  text_field :user_lastname, xpath: '//*[@id="user_lastname"]'
  text_field :user_mail, xpath: '//*[@id="user_mail"]'
  select_list :user_language, xpath: '//*[@id="user_language"]'
  button :send_registration_data, xpath: '//*[@type="submit"]'

  # Header
  link :home, xpath: '//*[@class="home"]'
  link :project, xpath: '//*[@class="projects"]'

  def register_random_user
    credetials = {
        username: Faker::Name.first_name,
        password: [*('A'..'Z')].sample(6).join,
        first_name: Faker::Superhero.name,
        last_name: Faker::Hipster.word.capitalize
    }
    self.user_login_element.when_present
    self.user_login = credetials[:username]
    self.user_password = credetials[:password]
    self.user_password_confirmation = credetials[:password]
    self.user_firstname = credetials[:first_name]
    self.user_lastname = credetials[:last_name]
    self.user_mail = Faker::Internet.email
    self.user_language = 'English'
    send_registration_data
    credetials
  end

end