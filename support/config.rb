#=========
# User config
#=========
USER_CONFIGS                              = YAML.load_file("#{Dir.pwd}/config.yml")
BASE_URL                                  = USER_CONFIGS[:base_url]

#=========
# RSpec config
#=========
RSpec.configure do |config|
  config.pattern                          = '**/*.rb'
end

#=========
# Front-end
#=========

# Page objects set


# PageObject config
PageObject.default_page_wait              = USER_CONFIGS[:timeout]
PageObject.default_element_wait           = USER_CONFIGS[:timeout]

# WebVisitor config
WebVisitor::Config.browser                 = USER_CONFIGS[:browser]
WebVisitor::Config.page_change_wait_time   = USER_CONFIGS[:timeout]
WebVisitor::Config.page_object_set         = "#{Dir.pwd}/resources/page_objects/main"