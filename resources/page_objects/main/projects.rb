
class Projects < WebVisitor::Page

  page_url BASE_URL + '/projects'
  uri_matcher /\/projects$/

  link :create_project, xpath: '//*[@class="icon icon-add"]'

  # Header
  link :home, xpath: '//*[@class="home"]'
  link :project, xpath: '//*[@class="projects"]'
  link :sign_out, xpath: '//*[@class="logout"]'
  div :logged_as, xpath: '//*[@id="loggedas"]'

end