
class PasswordChange < WebVisitor::Page

  page_url BASE_URL + '/my/password'
  uri_matcher /\/my\/password/

  text_field :old_password, xpath: '//*[@id="password"]'
  text_field :new_password, xpath: '//*[@id="new_password"]'
  text_field :new_password_confirmation, xpath: '//*[@id="new_password_confirmation"]'
  button :apply_password_change, xpath: '//*[@type="submit"]'

  # Header
  link :home, xpath: '//*[@class="home"]'
  link :project, xpath: '//*[@class="projects"]'
  link :sign_out, xpath: '//*[@class="logout"]'
  div :logged_as, xpath: '//*[@id="loggedas"]'

end
