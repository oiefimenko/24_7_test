
class Landing < WebVisitor::Page

  page_url BASE_URL
  uri_matcher /^#{BASE_URL}$/

  link :login, xpath: '//*[@id="account"]/.//*[@class="login"]'
  link :register, xpath: '//*[@id="account"]/.//*[@class="register"]'

  # Header
  link :home, xpath: '//*[@class="home"]'
  link :project, xpath: '//*[@class="projects"]'
  link :sign_out, xpath: '//*[@class="logout"]'
  div :logged_as, xpath: '//*[@id="loggedas"]'

end

