
class ProjectSettings < WebVisitor::Page

  uri_matcher /\/projects\/\w+\/settings/

  link :create_new_issue, xpath: '//*[@class="new-issue"]'
  link :overview, xpath: '//*[@class="overview"]'
  div :message, xpath: '//*[@id="flash_notice"]'

  # Members
  link :members, xpath: '//*[@id="tab-members"]'
  rows :members_list, xpath: '//*[@class="name user"]'
  link :new_member, xpath: '//*[@id="tab-content-members"]/p/a'
  text_field :member_search, xpath: '//input[@id="principal_search"]'
  labels :possible_members, xpath: '//*[@id="principals"]/label'
  checkbox :member_add, xpath: '//*[@id="principals"]/label[1]/input'
  checkbox :developer_role, xpath: '//*[@class="roles-selection"]/label[2]/input'
  button :complete_member_add, xpath: '//*[@id="member-add-submit"]'

  def add_user_to_project(username)
    self.members_element.when_present
    self.members
    self.new_member
    self.member_search_element.when_visible
    self.member_search = username
    wait_until { self.possible_members_elements.length == 1 }
    self.check_member_add
    self.check_developer_role
    self.complete_member_add
    wait_until { self.members_list_elements.length == 2 }
  end

end
