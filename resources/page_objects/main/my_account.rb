
class MyAccount < WebVisitor::Page

  page_url BASE_URL + '/my/account'
  uri_matcher /\/my\/account/

  div :message, xpath: '//*[@id="flash_notice"]'
  link :change_password, xpath: '//*[@class="icon icon-passwd"]'

  # Header
  link :home, xpath: '//*[@class="home"]'
  link :project, xpath: '//*[@class="projects"]'
  link :sign_out, xpath: '//*[@class="logout"]'
  div :logged_as, xpath: '//*[@id="loggedas"]'

end